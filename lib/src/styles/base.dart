import 'package:flutter/material.dart';

import 'colors.dart';

abstract class BaseStyle{

  static double get borderRadius=> 25;

  static double get borderWidth=> 2;

  static double get listFieldHorizontal => 25;

  static double get listFieldVertical => 8;

  static double get animationOffset=> 2;

  static EdgeInsets get listPadding{
    return EdgeInsets.symmetric(horizontal: listFieldHorizontal, vertical: listFieldVertical);
  }

  static List<BoxShadow> get boxShadow{
    return [
      BoxShadow(
        color: AppColors.darkgray.withOpacity(0.5),
        offset: Offset(1,2),
        blurRadius: 2,
      )
    ];
  }

  static List<BoxShadow> get boxShadowPressed{
    return [
      BoxShadow(
        color: AppColors.darkgray.withOpacity(0.5),
        offset: Offset(1,2),
        blurRadius: 1,
      )
    ];
  }

  static Widget iconPrefix(IconData icon) =>
      Padding(
        padding: const EdgeInsets.only(left: 8),
        child: Icon(
          icon,
          size: 35,
          color: AppColors.lightblue,
        ),
      );
}