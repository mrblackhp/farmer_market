import 'package:farmer_market/src/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'colors.dart';

abstract class TextStyles {
  static TextStyle get title => GoogleFonts.poppins(
      textStyle: TextStyle(
          color: AppColors.darkblue,
          fontSize: 40,
          fontWeight: FontWeight.bold));

  static TextStyle get subtitle => GoogleFonts.economica(
      textStyle: TextStyle(
          color: AppColors.straw, fontSize: 30, fontWeight: FontWeight.bold));

  static TextStyle get navTitle => GoogleFonts.poppins(
      textStyle:
          TextStyle(color: AppColors.darkblue, fontWeight: FontWeight.bold));

  static TextStyle get navTitleMaterial => GoogleFonts.poppins(
      textStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.bold));

  static TextStyle get body => GoogleFonts.roboto(
      textStyle: TextStyle(color: AppColors.darkgray, fontSize: 16));

  static TextStyle get picker => GoogleFonts.roboto(
      textStyle: TextStyle(color: AppColors.darkgray, fontSize: 35));

  static TextStyle get link => GoogleFonts.roboto(
      textStyle: TextStyle(
          color: AppColors.straw, fontSize: 16, fontWeight: FontWeight.bold));

  static TextStyle get suggestion => GoogleFonts.roboto(
      textStyle: TextStyle(color: AppColors.lightgray, fontSize: 14));

  static TextStyle get error => GoogleFonts.roboto(
      textStyle: TextStyle(color: AppColors.red, fontSize: 12));

  static TextStyle get buttonTextLight => GoogleFonts.roboto(
      textStyle: TextStyle(
          color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold));

  static TextStyle get buttonTextDark => GoogleFonts.roboto(
      textStyle: TextStyle(
          color: AppColors.darkgray,
          fontSize: 17,
          fontWeight: FontWeight.bold));
}
