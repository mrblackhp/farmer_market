import 'package:farmer_market/src/styles/colors.dart';
import 'package:farmer_market/src/styles/text.dart';
import 'package:flutter/material.dart';

import 'base.dart';

abstract class TextFieldStyles {
  static double get textBoxHorizontal => BaseStyle.listFieldHorizontal;

  static double get textBoxVertical => BaseStyle.listFieldVertical;

  static TextStyle get text => TextStyles.body;

  static TextStyle get placeholder => TextStyles.suggestion;

  static Color get cursorColor => AppColors.darkblue;

  static Widget iconPrefix(IconData icon) => BaseStyle.iconPrefix(icon);

  static TextAlign get textAlign => TextAlign.center;

  static BoxDecoration get cupertinoDecoration {
    return BoxDecoration(
      border: Border.all(
        color: AppColors.straw,
        width: BaseStyle.borderWidth,
      ),
      borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
    );
  }

  static BoxDecoration get cupertinoErrorDecoration {
    return BoxDecoration(
      border: Border.all(
        color: AppColors.red,
        width: BaseStyle.borderWidth,
      ),
      borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
    );
  }

  static InputDecoration materialDecoration(String hintText, IconData icon, String errorText) {
    return InputDecoration(
        contentPadding: EdgeInsets.all(8),
        hintText: hintText,
        hintStyle: TextFieldStyles.placeholder,
        border: InputBorder.none,
        errorText: errorText,
        errorStyle: TextStyles.error,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
          borderSide: BorderSide(
              color: AppColors.straw, width: BaseStyle.borderWidth),),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
        borderSide: BorderSide(
            color: AppColors.straw, width: BaseStyle.borderWidth),),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
        borderSide: BorderSide(
            color: AppColors.straw, width: BaseStyle.borderWidth),),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
        borderSide: BorderSide(
            color: AppColors.red, width: BaseStyle.borderWidth),),
      prefixIcon: iconPrefix(icon),
    );
  }
}
