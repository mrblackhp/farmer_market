import 'dart:async';

import 'package:farmer_market/src/models/product.dart';
import 'package:farmer_market/src/services/firestore_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uuid/uuid.dart';

class ProductBloc {
  final _productName = BehaviorSubject<String>();
  final _unitType = BehaviorSubject<String>();
  final _unitPrice = BehaviorSubject<String>();
  final _availableUnits = BehaviorSubject<String>();
  final _vendorId = BehaviorSubject<String>();

  final db = FirestoreService();
  var uuid = Uuid();

  //Get
  Stream<String> get productName =>
      _productName.stream.transform(validateProductName);

  Stream<String> get unitType => _unitType.stream;

  Stream<double> get unitPrice =>
      _unitPrice.stream.transform(validateUnitPrice);

  Stream<int> get availableUnits =>
      _availableUnits.stream.transform(validateAvailableUnits);

  Stream<bool> get isValid => Rx.combineLatest4(
      productName, unitType, unitPrice, availableUnits, (a, b, c, d) => true);

  //Set
  Function(String) get changeProductName => _productName.sink.add;

  Function(String) get changeUnitType => _unitType.sink.add;

  Function(String) get changeUnitPrice => _unitPrice.sink.add;

  Function(String) get changeAvailableInits => _availableUnits.sink.add;

  Function(String) get changeVendorId => _vendorId.sink.add;

  dispose() {
    _productName.close();
    _unitType.close();
    _unitPrice.close();
    _availableUnits.close();
    _vendorId.close();
  }

  Future<void> saveProduct() async {
    var product = Product(
      productName: _productName.value.trim(),
      unitType: _unitType.value,
      unitPrice: double.parse(_unitPrice.value),
      availableUnits: int.parse(_availableUnits.value),
      vendorId: _vendorId.value,
      productId: uuid.v4(),
      approved: true,
    );

    return await db
        .addProduct(product)
        .then((value) => print('Product Save'))
        .catchError((error) => print(error));
  }

  final validateUnitPrice = StreamTransformer<String, double>.fromHandlers(
      handleData: (unitPrice, sink) {
    try {
      sink.add(double.parse(unitPrice));
    } catch (e) {
      sink.addError('Must be a number');
    }
  });

  final validateAvailableUnits = StreamTransformer<String, int>.fromHandlers(
      handleData: (availableUnits, sink) {
    try {
      sink.add(int.parse(availableUnits));
    } catch (e) {
      sink.addError('Must be a number');
    }
  });

  final validateProductName = StreamTransformer<String, String>.fromHandlers(
      handleData: (productName, sink) {
    if (productName.length >= 3 && productName.length <= 20) {
      sink.add(productName);
    } else {
      if (productName.length < 3) {
        sink.addError('3 characters minimum');
      } else {
        sink.addError('20 characters maximum');
      }
    }
  });
}
