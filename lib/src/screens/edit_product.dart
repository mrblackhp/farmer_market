import 'dart:io';

import 'package:farmer_market/src/blocs/auth_bloc.dart';
import 'package:farmer_market/src/blocs/product_bloc.dart';
import 'package:farmer_market/src/models/user.dart';
import 'package:farmer_market/src/styles/base.dart';
import 'package:farmer_market/src/styles/colors.dart';
import 'package:farmer_market/src/styles/text.dart';
import 'package:farmer_market/src/widgets/button.dart';
import 'package:farmer_market/src/widgets/dropdown_button.dart';
import 'package:farmer_market/src/widgets/sliver_scaffold.dart';
import 'package:farmer_market/src/widgets/textfield.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class EditProduct extends StatefulWidget {
  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<ProductBloc>(context);
    final authBloc = Provider.of<AuthBloc>(context);

    return StreamProvider(
        create: (context) => authBloc.user,
        child: (Platform.isIOS)
            ? AppSliverScaffold.cupertinoSliverScaffold(
                '', pageBody(true, bloc, context), context)
            : AppSliverScaffold.materialSliverScaffold(
                '', pageBody(false, bloc, context), context));
  }

  Widget pageBody(bool isIOS, ProductBloc bloc, BuildContext context) {
    var items = Provider.of<List<String>>(context);
    return Consumer<User>(
      builder: (_, user, __) {
        if (user != null) bloc.changeVendorId(user.userId);
        return (user != null)
            ? ListView(
                children: <Widget>[
                  Text(
                    'Add Product',
                    style: TextStyles.subtitle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: BaseStyle.listPadding,
                    child: Divider(
                      height: 5,
                      color: AppColors.darkblue,
                    ),
                  ),
                  StreamBuilder<String>(
                      stream: bloc.productName,
                      builder: (context, snapshot) {
                        return AppTextField(
                          hintText: 'Prodcut Name',
                          cupertinoIcon: FontAwesomeIcons.shoppingBasket,
                          materialIcon: FontAwesomeIcons.shoppingBasket,
                          isIOS: isIOS,
                          errorText: snapshot.error,
                          onChange: bloc.changeProductName,
                        );
                      }),
                  StreamBuilder<String>(
                      stream: bloc.unitType,
                      builder: (context, snapshot) {
                        return AppDropdownButton(
                          hintText: 'Unit Type',
                          items: items,
                          value: snapshot.data,
                          onChanged: bloc.changeUnitType,
                          materialIcon: FontAwesomeIcons.balanceScale,
                          cupertinoIcon: FontAwesomeIcons.balanceScale,
                        );
                      }),
                  StreamBuilder<double>(
                      stream: bloc.unitPrice,
                      builder: (context, snapshot) {
                        return AppTextField(
                          hintText: 'Unit Price',
                          cupertinoIcon: FontAwesomeIcons.tag,
                          materialIcon: FontAwesomeIcons.tag,
                          isIOS: isIOS,
                          textInputType: TextInputType.number,
                          errorText: snapshot.error,
                          onChange: bloc.changeUnitPrice,
                        );
                      }),
                  StreamBuilder<int>(
                      stream: bloc.availableUnits,
                      builder: (context, snapshot) {
                        return AppTextField(
                          hintText: 'Avaialbe Units',
                          cupertinoIcon: FontAwesomeIcons.cube,
                          materialIcon: FontAwesomeIcons.cube,
                          isIOS: isIOS,
                          textInputType: TextInputType.number,
                          errorText: snapshot.error,
                          onChange: bloc.changeAvailableInits,
                        );
                      }),
                  AppButton(
                    buttonType: ButtonType.Straw,
                    buttonText: 'Add Image',
                    onPressed: () => print('Add Image'),
                  ),
                  StreamBuilder<bool>(
                      stream: bloc.isValid,
                      builder: (context, snapshot) {
                        return AppButton(
                          buttonType: snapshot.data == true
                              ? ButtonType.DarkBlue
                              : ButtonType.Disable,
                          buttonText: 'Save Product',
                          onPressed: bloc.saveProduct,
                        );
                      }),
                ],
              )
            : Container();
      },
    );
  }
}
