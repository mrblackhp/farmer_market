import 'dart:async';
import 'dart:io';
import 'package:farmer_market/src/blocs/auth_bloc.dart';
import 'package:farmer_market/src/styles/tabbar.dart';
import 'package:farmer_market/src/widgets/navbar.dart';
import 'package:farmer_market/src/widgets/orders.dart';
import 'package:farmer_market/src/widgets/products.dart';
import 'package:farmer_market/src/widgets/profile.dart';
import 'package:farmer_market/src/widgets/vendor_scaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Vendor extends StatefulWidget {


  @override
  _VendorState createState() => _VendorState();

  static TabBar get venderTabBar {
    return TabBar(
        unselectedLabelColor: TabBarStyles.unselectedLabelColor,
        labelColor: TabBarStyles.labelColor,
        indicatorColor: TabBarStyles.indicatorColor,
        tabs: <Widget>[
          Tab(icon: Icon(Icons.list)),
          Tab(icon: Icon(Icons.shopping_cart)),
          Tab(icon: Icon(Icons.person)),
        ]);
  }
}

class _VendorState extends State<Vendor> {
  StreamSubscription _streamSubscription;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      var authBloc = Provider.of<AuthBloc>(context, listen: false);
      _streamSubscription = authBloc.user.listen((user) {
        if (user == null)
          Navigator.pushNamedAndRemoveUntil(
              context, '/login', (route) => false);
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              AppNavbar.cupertinoSliverNavBar(title: 'Vendor Name',context: context),
            ];
          },
          body: Center(
            child: VendoScaffold.cupertinoTabScaffold,
          ),
        ),
      );
    } else {
      return DefaultTabController(
        length: 3,
        child: Scaffold(
          body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                AppNavbar.materialNavBar(
                    title: 'Vendor Name', tabBar: Vendor.venderTabBar)
              ];
            },
            body: TabBarView(
              children: [
                Products(),
                Orders(),
                Profile(),
              ],
            ),
          ),
        ),
      );
    }
  }
}
