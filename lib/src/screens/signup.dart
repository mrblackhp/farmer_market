import 'dart:async';
import 'dart:io';

import 'package:farmer_market/src/blocs/auth_bloc.dart';
import 'package:farmer_market/src/styles/base.dart';
import 'package:farmer_market/src/widgets/alerts.dart';
import 'package:farmer_market/src/widgets/button.dart';
import 'package:farmer_market/src/widgets/social_button.dart';
import 'package:farmer_market/src/widgets/textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../styles/base.dart';
import '../styles/text.dart';

class Signup extends StatefulWidget {

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {

  StreamSubscription _userSubscription;
  StreamSubscription _errorMessageSubcription;

  @override
  void initState() {
    final authBloc = Provider.of<AuthBloc>(context, listen: false);
    _userSubscription = authBloc.user.listen((user) {
      if(user != null)
        Navigator.pushReplacementNamed(context, '/landing');
    });

    _errorMessageSubcription =
        authBloc.errorMessage.listen((errorMessage) {
          if (errorMessage != '') {
            AppAlerts.showErrorDialog(Platform.isIOS,  context, errorMessage)
                .then((_) => authBloc.clearErrorMessage());
          }
        });
    super.initState();
  }

  @override
  void dispose() {
    _userSubscription.cancel();
    _errorMessageSubcription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);
    if (Platform.isIOS) {
      return CupertinoPageScaffold(
        child: pageBody(context,authBloc),
      );
    } else {
      return Scaffold(
        body: pageBody(context,authBloc),
      );
    }
  }

  Widget pageBody(BuildContext context, AuthBloc authBloc) {
    final authBloc = Provider.of<AuthBloc>(context);
    return ListView(
      padding: EdgeInsets.all(0),
      children: [
        Container(
          height: MediaQuery.of(context).size.height * 0.2,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/top_bg.png'),
                fit: BoxFit.fill),
          ),
        ),
        Container(
          height: 200,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/logo.png'),
            ),
          ),
        ),
        StreamBuilder<String>(
            stream: authBloc.email,
            builder: (context, snapshot) {
              return AppTextField(
                isIOS: Platform.isIOS,
                hintText: 'Email',
                materialIcon: CupertinoIcons.mail_solid,
                cupertinoIcon: Icons.email,
                textInputType: TextInputType.emailAddress,
                errorText: snapshot.error,
                onChange: authBloc.changeEmail,
              );
            }
        ),
        StreamBuilder<String>(
            stream: authBloc.password,
            builder: (context, snapshot) {
              return AppTextField(
                isIOS: Platform.isIOS,
                hintText: 'Password',
                materialIcon: Icons.lock,
                cupertinoIcon: CupertinoIcons.padlock_solid,
                textInputType: TextInputType.emailAddress,
                obscureText: true,
                errorText: snapshot.error,
                onChange: authBloc.changePassword,
              );
            }
        ),
        StreamBuilder<bool>(
            stream: authBloc.isValid,
            builder: (context, snapshot) {
              return AppButton(
                buttonText: 'Signup',
                buttonType: (snapshot.data == true) ? ButtonType.LightBlue : ButtonType.Disable,onPressed: authBloc.signupEmail,
              );
            }
        ),
        SizedBox(height: 6,),
        Center(child: Text('Or', style: TextStyles.suggestion)),
        SizedBox(height: 6,),
        Padding(
          padding: BaseStyle.listPadding,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AppSocialButton(
                socialType: SocialType.Facebook,
              ),
              SizedBox(width: 15,),
              AppSocialButton(
                socialType: SocialType.Google,
              ),
            ],
          ),
        ),
        Padding(
          padding: BaseStyle.listPadding,
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Already Have an Account? ',
              style: TextStyles.body,
              children: [
                TextSpan(
                    text: 'Login',
                    style: TextStyles.link,
                    recognizer: TapGestureRecognizer()
                      ..onTap = () => Navigator.pushNamed(context, '/login')),
              ],
            ),
          ),
        )
      ],
    );
  }
}