import 'package:farmer_market/src/screens/edit_product.dart';
import 'package:farmer_market/src/screens/landing.dart';
import 'package:farmer_market/src/screens/login.dart';
import 'package:farmer_market/src/screens/signup.dart';
import 'package:farmer_market/src/screens/vendor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class Routes {
  static MaterialPageRoute materialPageRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/landing':
        return MaterialPageRoute(builder: (context) => Landing());
        break;
      case '/signup':
        return MaterialPageRoute(builder: (context) => Signup());
        break;
      case '/login':
        return MaterialPageRoute(builder: (context) => Login());
        break;
      case '/vendor':
        return MaterialPageRoute(builder: (context) => Vendor());
        break;
      case '/editproduct':
        return MaterialPageRoute(builder: (context) => EditProduct());
        break;
      default:
        return MaterialPageRoute(builder: (context) => Login());
        break;
    }
  }

  static CupertinoPageRoute cupertinoRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/landing':
        return CupertinoPageRoute(builder: (context) => Landing());
        break;
      case '/signup':
        return CupertinoPageRoute(builder: (context) => Signup());
        break;
      case '/login':
        return CupertinoPageRoute(builder: (context) => Login());
        break;
      case '/vendor':
        return CupertinoPageRoute(builder: (context) => Vendor());
        break;
      case '/editproduct':
        return CupertinoPageRoute(builder: (context) => EditProduct());
        break;
      default:
        return CupertinoPageRoute(builder: (context) => Login());
        break;
    }
  }
}
