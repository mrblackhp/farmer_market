import 'package:farmer_market/src/styles/buttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../styles/base.dart';
import '../styles/colors.dart';
import '../styles/text.dart';

class AppButton extends StatefulWidget {
  final String buttonText;
  final ButtonType buttonType;
  final void Function() onPressed;

  AppButton({@required this.buttonText, this.buttonType, this.onPressed});

  @override
  _AppButtonState createState() => _AppButtonState();
}

class _AppButtonState extends State<AppButton> {
  bool pressed = false;

  @override
  Widget build(BuildContext context) {
    TextStyle fontStyle;
    Color buttonColor;

    switch (widget.buttonType) {
      case ButtonType.Straw:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.straw;
        break;
      case ButtonType.LightBlue:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.lightblue;
        break;
      case ButtonType.DarkBlue:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.darkblue;
        break;
      case ButtonType.Disable:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.lightgray;
        break;
      case ButtonType.DarkGray:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.darkgray;
        break;
      default:
        fontStyle = TextStyles.buttonTextLight;
        buttonColor = AppColors.lightblue;
        break;
    }

    return AnimatedContainer(
      padding: EdgeInsets.only(
        top: pressed ? BaseStyle.listFieldVertical + BaseStyle.animationOffset : BaseStyle.listFieldVertical,
        bottom: pressed ? BaseStyle.listFieldVertical - BaseStyle.animationOffset : BaseStyle.listFieldVertical,
        left: BaseStyle.listFieldHorizontal,
        right: BaseStyle.listFieldHorizontal,
      ),
      child: GestureDetector(
        child: Container(
          height: ButtonStyles.buttonHeight,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
            boxShadow: pressed ? BaseStyle.boxShadowPressed : BaseStyle.boxShadow,
          ),
          child: Center(
            child: Text(
              widget.buttonText,
              style: fontStyle,
            ),
          ),
        ),
        onTapDown: (details){
          setState(() {
            if(widget.buttonType != ButtonType.Disable) pressed = !pressed;
          });
        },
        onTapUp: (details){
          setState(() {
            if(widget.buttonType != ButtonType.Disable) pressed = !pressed;
          });
        },
        onTap: (){
          if(widget.buttonType != ButtonType.Disable){
            widget.onPressed();
          }
        },
      ),
      duration: Duration(milliseconds: 20),
    );
  }
}

enum ButtonType { LightBlue, Straw, Disable, DarkGray, DarkBlue }
