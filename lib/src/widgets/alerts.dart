import 'package:farmer_market/src/styles/text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class AppAlerts {
  static Future<void> showErrorDialog(
      bool isIOS, BuildContext context, String errorMessage) async {
    return isIOS ?  showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Text(
              'ERROR',
              style: TextStyles.subtitle,
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(
                    errorMessage,
                    style: TextStyles.body,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              CupertinoButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    'Okay',
                    style: TextStyles.body,
                  )),
            ],
          );
        })
        : showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'ERROR',
              style: TextStyles.subtitle,
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(
                    errorMessage,
                    style: TextStyles.body,
                  )
                ],
              ),
            ),
            actions: <Widget>[
              RaisedButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    'Okay',
                    style: TextStyles.body,
                  )),
            ],
          );
        });
  }
}
