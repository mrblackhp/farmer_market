import 'package:farmer_market/src/styles/base.dart';
import 'package:farmer_market/src/styles/buttons.dart';
import 'package:farmer_market/src/styles/colors.dart';
import 'package:farmer_market/src/styles/text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';

class AppDropdownButton extends StatelessWidget {
  final List<String> items;
  final String hintText;
  final IconData cupertinoIcon;
  final IconData materialIcon;
  final String value;
  final Function(String) onChanged;

  AppDropdownButton({
    @required this.items,
    @required this.hintText,
    this.cupertinoIcon,
    this.materialIcon,
    this.value,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return Padding(
        padding: BaseStyle.listPadding,
        child: Container(
          height: ButtonStyles.buttonHeight,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
            border: Border.all(
                color: AppColors.straw, width: BaseStyle.borderWidth),
          ),
          child: Row(
            children: <Widget>[
              Container(
                width: 35,
                child: BaseStyle.iconPrefix(materialIcon),
              ),
              Expanded(
                child: Center(
                  child: GestureDetector(
                    child: value == null ? Text(
                      hintText,
                      style: TextStyles.suggestion,
                    ) : Text(
                      value,
                      style: TextStyles.body,
                    ),
                    onTap: () {
                      showCupertinoModalPopup(
                          context: context,
                          // ignore: missing_return
                          builder: (BuildContext context) {
                            _selectIOS(context, items);
                          });
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Padding(
        padding: BaseStyle.listPadding,
        child: Container(
          height: ButtonStyles.buttonHeight,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(BaseStyle.borderRadius),
            border: Border.all(
                color: AppColors.straw, width: BaseStyle.borderWidth),
          ),
          child: Row(
            children: <Widget>[
              Container(
                width: 35,
                child: BaseStyle.iconPrefix(materialIcon),
              ),
              Expanded(
                child: Center(
                  child: DropdownButton<String>(
                    items: buildMaterialItems(items),
                    value: value,
                    hint: Text(
                      hintText,
                      style: TextStyles.suggestion,
                    ),
                    style: TextStyles.body,
                    underline: Container(),
                    iconEnabledColor: AppColors.straw,
                    onChanged: (value) => onChanged(value),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  List<DropdownMenuItem<String>> buildMaterialItems(List<String> items) {
    return items?.map((item) => DropdownMenuItem<String>(
              child: Text(
                item,
                textAlign: TextAlign.center,
              ),
              value: item,
            ))
        ?.toList() ?? [];
  }

  List<Widget> buildCupertinoItems(List<String> items) {
    return items
        .map((item) => Text(
              item,
              textAlign: TextAlign.center,
              style: TextStyles.picker,
            ))
        .toList();
  }

  _selectIOS(BuildContext context, List<String> items) {
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(),
      child: Container(
        color: Colors.white,
        height: 200,
        child: CupertinoPicker(
          itemExtent: 45,
          onSelectedItemChanged: (int index) => onChanged(items[index]),
          children: buildCupertinoItems(items),
          diameterRatio: 1,
        ),
      ),
    );
  }
}
